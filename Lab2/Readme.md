# Lab 2 - Reports with Apache Spark
## Установка и запуск (все команды в *start.txt*)
1. cd (путь к папке с проектом)
2. Туда положить файл *docker-compose.yml*
3. В консоли запустить docker-compose up --build -d
4. После установки и запуска можно входить c помощью
*  ssh root@localhost -p 2222 
*  mapr
5. Перейти в папку, которую указали в docker-compose.yml
6. cd /home/mapr/lab_2
7. Команды для скачивания всего необходимого:  
echo 'export PATH=$PATH:/opt/mapr/spark/spark-3.2.0/bin' > /root/.bash_profile  
source /root/.bash_profile  
*apt-get update && apt-get install -y python3-distutils python3-apt*  
*wget https://bootstrap.pypa.io/pip/3.6/get-pip.py*  
*python3 get-pip.py*  
*pip install jupyter* 
*pip install pyspark* 
8. Запустить блокнот с помощью:  
*jupyter-notebook --ip=0.0.0.0 --port=50001 --allow-root --no-browser*
9. Открыть в браузере

## Топ 10 языков программирования с 2010 по 2020:
	_____________
	|    2010   |
	+-----------+
	|   Language|
	+-----------+
	|       Java|
	| JavaScript|
	|        PHP|
	|     Python|
	|Objective-C|
	|          C|
	|       Ruby|
	|     Delphi|
	|       Bash|
	|          R|
	+-----------+

	_____________
	|    2011   |
	+-----------+
	|   Language|
	+-----------+
	|        PHP|
	|       Java|
	| JavaScript|
	|     Python|
	|Objective-C|
	|          C|
	|       Ruby|
	|       Perl|
	|     Delphi|
	|       Bash|
	+-----------+

	_____________
	|    2012   |
	+-----------+
	|   Language|
	+-----------+
	|        PHP|
	| JavaScript|
	|       Java|
	|     Python|
	|Objective-C|
	|          C|
	|       Ruby|
	|          R|
	|       Bash|
	|      Scala|
	+-----------+

	_____________
	|    2013   |
	+-----------+
	|   Language|
	+-----------+
	| JavaScript|
	|       Java|
	|        PHP|
	|     Python|
	|Objective-C|
	|          C|
	|       Ruby|
	|          R|
	|       Bash|
	|      Scala|
	+-----------+

	_____________
	|    2014   |
	+-----------+
	|   Language|
	+-----------+
	| JavaScript|
	|       Java|
	|        PHP|
	|     Python|
	|          C|
	|Objective-C|
	|          R|
	|       Ruby|
	|     MATLAB|
	|       Bash|
	+-----------+

	_____________
	|    2015   |
	+-----------+
	|   Language|
	+-----------+
	| JavaScript|
	|       Java|
	|        PHP|
	|     Python|
	|          R|
	|          C|
	|Objective-C|
	|       Ruby|
	|     MATLAB|
	|      Scala|
	+-----------+

	_____________
	|    2016   |
	+----------+
	|  Language|
	+----------+
	|JavaScript|
	|      Java|
	|    Python|
	|       PHP|
	|         R|
	|         C|
	|      Ruby|
	|     Scala|
	|      Bash|
	|    MATLAB|
	+----------+

	_____________
	|    2017   |
	+-----------+
	|   Language|
	+-----------+
	| JavaScript|
	|       Java|
	|     Python|
	|        PHP|
	|          R|
	|          C|
	|Objective-C|
	|       Ruby|
	| PowerShell|
	| TypeScript|
	+-----------+

	_____________
	|    2018   |
	+----------+
	|  Language|
	+----------+
	|    Python|
	|JavaScript|
	|      Java|
	|       PHP|
	|         R|
	|         C|
	|     Scala|
	|TypeScript|
	|PowerShell|
	|      Bash|
	+----------+

	_____________
	|    2019   |
	+----------+
	|  Language|
	+----------+
	|    Python|
	|JavaScript|
	|      Java|
	|       PHP|
	|         R|
	|         C|
	|        Go|
	|      Dart|
	|    MATLAB|
	|      Bash|
	+----------+