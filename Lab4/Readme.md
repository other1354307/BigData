# Lab 4 - Zookeeper 
### Используется библиотеку kazoo - аналог ZooKeeper на python.
## Решение
### Philosophers
#### Задание
Решите проблему обедающих философов (каждый философ - отдельный процесс в системе)
#### Описание
1. Каждый философ - объект класса *Philosopher*, который в себе содержит свой id, id левой и правой вилки, id левого и правого соседа, имя задачи и "путь" до вилок.
2. В методе *run* определяем замки на стол и свои вилки
3. Философы едят только *eat_seconds* с начала отсчёта
4. Берём вилки, только если они свободны, и философ поел не больше, чем его соседи
5. Если вилки взяты, то едим, сообщаем об этом, увеличиваем счетчик количества приема пищи и кладем вилки, иначе думаем
6. Задаём параметры и создаём процессы философов
7. Храним количество приёмов пищи в общем листе, для того чтобы он синхронизировался вызываем Manager. 
8. Создаём и "запускаем" каждого философа
#### Результат 
	...
	Philosopher 1: Im thinking
	Philosopher 2: Im thinking
	Philosopher 4: Im thinking
	Philosopher 1: Im thinking
	Philosopher 2: Im thinking
	Philosopher 4: Im thinking
	Philosopher 1: Im thinking
	Philosopher 2: Im thinking
	Philosopher 4: Im thinking
	Philosopher with id=1 eat counter=7
	Philosopher with id=0 eat counter=8
	Philosopher with id=2 eat counter=8
	Philosopher with id=4 eat counter=7
	Philosopher with id=3 eat counter=8
	
### 2PC
#### Задание
Реализуйте двуфазный коммит протокол для high-available регистра (каждый регистр - отдельный процесс в системе)
#### Описание
1. Создаём координатор
2. Храним лист, чтобы знать заходил ли уже клиент
3. Создаём узлы
4. Попадаем в *watch_clients* по дефолту без аргументов
5. Запускаем клиентов
6. Клиенты хранят путь до своего узла и свой id
7. Клиенты рандомно выбирают свой ответ и пишут о своём решении
8. Клиенты сохраняют результат на своём узле и ждут
9. Также они параллельно смотрят за изменением своего узла
10. Если они долго ждут, то останавливаются
11. Как только подключается клиент, в листе проставляем True
12. Если клиент отключился, то отсылаем остальным клиентам, что надо отключиться
13. Если подключились все клиенты, то принимаем решение.
14. Как только оно принято, рассылаем его на все узлы, соответсвенно тригерим DataWatch у клиентов. 
#### Результат
	[False, False, False]
	Waiting for the others. clients=[]
	Client 0 request commit
	Client 0 create
	Client 0 triggered 0
	Client 0 run
	[True, False, False]
	Waiting for the others. clients=['0']
	Client 1 request rollback
	Client 1 create
	Client 1 triggered 0
	Client 1 run
	[True, True, False]
	Waiting for the others. clients=['0', '1']
	Client 2 request rollback
	Client 2 create
	Client 2 triggered 0
	Client 2 run
	[True, True, True]
	Check clients
	check_clients()
	Client 0 triggered 1Client 1 triggered 1Client 2 triggered 1


	Client 0 do rollbackClient 1 do rollbackClient 2 do rollback


	Client 0 runClient 1 runClient 2 run


	Client 0 stop
	[True, True, True]
	Waiting for the others. clients=['1', '2']
	Client 1 exitClient 2 exit

	Один отключился, отключайся!!!
	[True, True, True]
	Waiting for the others. clients=[]
